//
//  LoginViewController.swift
//  appclimagr2
//
//  Created by AlejandroPardina on 27/10/17.
//  Copyright © 2017 AlejandroPardina. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    //MARK Outlets
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var userPassword: UITextField!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK Actions
    @IBAction func entrarButton(_ sender: Any) {
        let user = userName.text ?? ""
        let password = userPassword.text ?? ""
        
        switch (user, password) {
        case ("sebas", "sebas"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        case ("sebas", _):
            mostrarAlerta(mensaje: "contaseña incorrecta")
            //print("contaseña incorrecta")
        default:
            mostrarAlerta(mensaje: "usuario y contraseña incorrectas")
            //print("usuario y contraseña incorrectas")
        }
        
    }
    private func mostrarAlerta(mensaje: String)
    {
        let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default)
        {
            (action) in
            self.userName.text = ""
            self.userPassword.text = ""
            
            
        }
        alertView.addAction(aceptar)
        present(alertView,animated: true,completion: nil)
    }
    
    @IBAction func invitadoEntrarButton(_ sender: Any) {
    }

}
