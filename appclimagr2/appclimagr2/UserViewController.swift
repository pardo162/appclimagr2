//
//  UserViewController.swift
//  appclimagr2
//
//  Created by AlejandroPardina on 1/11/17.
//  Copyright © 2017 AlejandroPardina. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var consultarButtonPressed: UIButton!
    
    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        let servicio = Servicio()
        servicio.consultarPorCiudada(ciudad: cityTextField.text!) {(weather)in DispatchQueue.main.async {
            self.weatherLabel.text = weather
            }
            
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
