//
//  Servicio.swift
//  appclimagr2
//
//  Created by AlejandroPardina on 7/11/17.
//  Copyright © 2017 AlejandroPardina. All rights reserved.
//

import UIKit
import Foundation
class Servicio{

    func consultarPorCiudada(ciudad: String, completion:@escaping (String) -> ())
    {
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(ciudad ?? "quito")&appid=f4b14259b70ff9328f483a7729e0c980"
        consultar(urlStr: urlStr ){(weather ,ciudad) in completion (weather)}
    }
    func consultarPorUbicacion(lat:Double, lon:Double, completion:@escaping (String,String) -> ())
    {
        let urlString = "http://samples.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=ca98a02446d275314d44853a631d6f38"
        consultar(urlStr: urlString ){(weather ,ciudad) in completion (weather,ciudad)}
        
    }
    func consultar(urlStr: String , completion:@escaping (String,String) -> ())
    {
        let url = URL(string: urlStr)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) {(data,response, error) in
            if let _ = error {
                return
            }
            let weatherData = data as! Data
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                //print(weatherJson)
                let weatherArray = weatherDictionary["weather"] as! NSArray
                let ciudad = weatherDictionary["name"] ?? "error"
                //print(ciudad )
                let weather = weatherArray[0] as! NSDictionary
                let weatherDesc = weather["description"] ?? "error"
                
                    completion ( "\(weatherDesc)","\(ciudad)")
                
                
            }catch{print("Erro JSON")}
            
        }
        task.resume()
    
    }
}
