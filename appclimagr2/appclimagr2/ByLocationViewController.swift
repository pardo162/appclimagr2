//
//  ByLocationViewController.swift
//  appclimagr2
//
//  Created by AlejandroPardina on 27/10/17.
//  Copyright © 2017 AlejandroPardina. All rights reserved.
//

import UIKit
import CoreLocation

class ByLocationViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var weatherLabel: UILabel!
    
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
        
        //getWeatherByLocation()
        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate

        getWeatherByLocation(
            lat: (location?.latitude)!,
            lon: (location?.longitude)!
        )
        // pare de consultar 
        locationManager.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getWeatherByLocation(lat:Double, lon:Double) {
 
        let servicio = Servicio()
        servicio.consultarPorUbicacion(lat: lat , lon: lon) {(weather, ciudad) in
            DispatchQueue.main.async {
                self.weatherLabel.text = weather + " "+ciudad
            
            }
            
        }
    }
    

}
